# el-repo-grande

| file | file type | local filesize |
| ---- | --- | --- |
| codium.deb | debian package| 63M |
| export.tar.gz | tar.gz archive | 59M |
| music.mp3 | music | 37M |
| art-of-community.pdf | pdf document | 21M |
| bfg.jar | java package | 13M |
| topgrade | binary program| 10M |
| README.md | markdown | 18b |
